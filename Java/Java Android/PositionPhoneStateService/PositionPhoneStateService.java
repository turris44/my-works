package it.ads.etas.android.agent.service;

import it.ads.etas.android.agent.R;
import it.ads.etas.android.agent.util.PositionInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

 
public class PositionPhoneStateService extends Service {

	private LocationManager locationManager;
	SharedPreferences etasPreference;
	Boolean isGPSEnabled;
	Long distanceUpdateRange;
	Long minimumTimeDistanceUpdate;
	Boolean isNetworkEnabled;
	List<PositionInfo> positionInfos = new ArrayList<PositionInfo>();

	@Override
	public void onCreate() {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		etasPreference = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i("debug", "PositionService Start");
		distanceUpdateRange = Long.decode(etasPreference.getString(
				getString(R.string.key_distance_settings), null));
		minimumTimeDistanceUpdate = Long.decode(etasPreference.getString(
				getString(R.string.key_minimum_time_settings), null));
		isGPSEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);
		isNetworkEnabled = locationManager
				.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		if (!isNetworkEnabled && !isGPSEnabled) {
			Toast.makeText(getBaseContext(), "Impossibile rilevare la posizione",
					Toast.LENGTH_SHORT).show();
		}
		if (isNetworkEnabled) {
			locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER,
					minimumTimeDistanceUpdate, distanceUpdateRange,
					locationListenerNetwork);
		} else if (isGPSEnabled) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, minimumTimeDistanceUpdate,
					distanceUpdateRange, locationListenerGps);
		}
		return super.onStartCommand(intent, flags, startId);
	}

	LocationResult locationResult = new LocationResult() {
		@Override
		public void gotLocation(Location location) {
			if (location != null) {
				PositionInfo positionInfo = new PositionInfo();
				positionInfo.setLongitude(location.getLongitude());
				positionInfo.setLatitude(location.getLatitude());
				positionInfo.setPositionDate(new Date(location.getTime()));
				positionInfos.add(positionInfo);
				Log.i("debug", "Posizione salvata");
				for (PositionInfo positionInfo1 : positionInfos) {
					Log.i("debug", positionInfo1.getLatitude() + " "
							+ positionInfo1.getLongitude() + " "
							+ positionInfo1.getPositionDate());
				}
			} else
				Log.i("debug", "Posizione non salvata");
		}
	};

	LocationListener locationListenerGps = new LocationListener() {
		public void onLocationChanged(Location location) {
			String msg = "Latitudine: " + location.getLatitude()
					+ "Longitudine: " + location.getLongitude();
			Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
			locationResult.gotLocation(location);
		}

		public void onProviderDisabled(String provider) {
			Log.i("debug", "GPSDisabled");
		}

		public void onProviderEnabled(String provider) {
			Log.i("debug", "GPSEnabled");
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
			Log.i("debug", "onStatusChanged");
		}
	};

	LocationListener locationListenerNetwork = new LocationListener() {
		public void onLocationChanged(Location location) {
			String msg = "Latitudine: " + location.getLatitude()
					+ "Longitudine: " + location.getLongitude();
			Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
			locationResult.gotLocation(location);
		}

		public void onProviderDisabled(String provider) {
			Log.i("debug", "NetworkDisabled");
		}

		public void onProviderEnabled(String provider) {
			Log.i("debug", "NetworkEnabled");
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
			Log.i("debug", "onStatusNetworkChanged");
		}
	};

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		Log.i("debug", "Service destroyed");
		super.onDestroy();
	}

	public static abstract class LocationResult {
		public abstract void gotLocation(Location location);
	}

}